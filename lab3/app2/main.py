import json
import socket

from datetime import datetime

from flask import Flask


app = Flask(__name__)


@app.route("/")
def hello():
    date_obj = datetime.now()
    resp_dict = {}

    resp_dict['hostname'] = socket.gethostname()
    resp_dict['timestamp'] = date_obj.strftime("%Y%m%d-%H:%M:%S.%f")

    return json.dumps(resp_dict)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
