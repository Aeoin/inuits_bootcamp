### app2


```
$ sudo apt install python3 python3-pip
```


```
$ pipenv install
```


```
$ pipenv run gunicorn main:app
```


```
$ pipenv run gunicorn --bind 0.0.0.0:5000 --access-logfile - main:app
```


#### References

* https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04
