# Lab 1

# Provisioning VM

```sh
vagrant init centos/7
vagrant up
```

# Installing features

Installing python3

```sh
sudo yum install -y python3
```

Creating admin user with sudo rights

```sh
sudo adduser admin
sudo passwd admin
sudo usermod -aG wheel admin
```