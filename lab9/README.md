# Create the image for the webserver
```sh
$ docker build -t webserver .
```

# Run docker-compose with the amount of backend servers needed
```sh
$ docker-compose -d --scale webapp=2
```


