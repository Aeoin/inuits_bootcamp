# Create the image
```sh
$ docker build -t nginx-http-webserver .
```

# Next run the container with the local path to this map
```sh
$ docker run -d --rm -ti -p 80:80 -v </full/path/to/this/map>:/usr/share/nginx/data nginx-http-webserver
```


